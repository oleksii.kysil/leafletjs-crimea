# Leafletjs Crimea

Small version of using leafletjs with having correct borders of Ukraine.

## Description
This is small and dirty demo on how to merge default OSM tile with the one provided by OSM Ukraine community, so that Ukraine is displayed in its correct borders.
What this small snippet of JS does - is basically forcing leaflet to use OSM Ukraine tiles when rendering map around Crimea.
By that the correct rendering is achieved, however it might lead for some issues.

![](images/demo.png)

## Known issues
1. Might have problems with displaying map at zoom value 9-10;
2. Depending on the styling and configuration part of the map might be greyed out on some zoom levels.

## Further steps
This might be extended to be a fully production-ready leafletjs plugin, so that it can be easily added by other users.
Please feel free to update it with a better solution.


