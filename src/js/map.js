function showMap() {
    var mapOptions = {
        zoom: 10,
        minZoom: 8,
        center: [44.95, 34.1]
    }
    var map = new L.map('mapid', mapOptions);
    var layerUp = new L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
        bounds :  L.latLngBounds(L.latLng(90, -180), L.latLng( 49, 180))
    });
    var layerLeft = new L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
        bounds :  L.latLngBounds(L.latLng(49, -180), L.latLng( 41, 29))
    });
    var layerRight = new L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
        bounds :  L.latLngBounds(L.latLng(49, 180), L.latLng( 44, 36.649))
    });
    var layerDown = new L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
        bounds :  L.latLngBounds(L.latLng(41, -180), L.latLng( -90, 180))
    });

    var layerUa = L.tileLayer('https://tiles.openstreetmap.org.ua/osm/{z}/{x}/{y}.png', {
        attribution: '&copy; <a href="https://www.openstreetmap.org.ua">OpenStreetMap Україна</a> contributors',
        maxZoom: 18,
        bounds :  L.latLngBounds(L.latLng(49, 29), L.latLng( 44, 36.649))
    });

    map.addLayer(layerUp);
    map.addLayer(layerLeft);
    map.addLayer(layerRight);
    map.addLayer(layerDown);
    map.addLayer(layerUa);
}